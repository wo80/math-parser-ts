import { describe, expect, it } from 'vitest'
import Scanner from '../src/parser/Scanner'

const numbers: { test: string, expected: number }[] = [
  { "test": "1.0", "expected": 1.0 },
  { "test": ".50", "expected": 0.50 },
  { "test": "2.0e2", "expected": 2.0e2 },
  { "test": "2.0e-2", "expected": 2.0e-2 },
  { "test": "2.0e+2", "expected": 2.0e+2 },
  { "test": "2.0e-02", "expected": 2.0e-02 },
  { "test": "2e05", "expected": 2e05 },
  { "test": "2e-05", "expected": 2e-05 }
];

const expressions: { test: string, expected: number[] }[] = [
  { "test": "1.0", "expected": [0] },
  { "test": "-2.0", "expected": [1, 0] },
  { "test": "1+1", "expected": [0, 1, 0] },
  { "test": "1--1", "expected": [0, 1, 1, 0] },
  { "test": "-2+4", "expected": [1, 0, 1, 0] },
  { "test": "3^2", "expected": [0, 1, 0] },
  { "test": "3^-2", "expected": [0, 1, 1, 0] },
  { "test": "3^2^3", "expected": [0, 1, 0, 1, 0] },
  { "test": "(3^2)^3", "expected": [2, 0, 1, 0, 2, 1, 0] },
  { "test": "cos(1)", "expected": [4, 2, 0, 2] },
  { "test": "min(2, 3)", "expected": [4, 2, 0, 5, 0, 2] },
  { "test": "sin(pi/2)", "expected": [4, 2, 3, 1, 0, 2] },
  { "test": "pi*sqrt(4*x^2)", "expected": [3, 1, 4, 2, 0, 1, 3, 1, 0, 2] },
  { "test": "-log(sin(pi/2)^2)^2", "expected": [1, 4, 2, 4, 2, 3, 1, 0, 2, 1, 0, 2, 1, 0] }
];

const invalid: { test: string, expected: number[] }[] = [
  { "test": " 1.1.1 ", "expected": [0, 0] },
  { "test": " 1,1 ", "expected": [0, 5, 0] },
  { "test": "-(+)-", "expected": [1, 2, 1, 2, 1] },
  { "test": "1   1", "expected": [0, 0] },
  { "test": "1+*+1", "expected": [0, 1, 1, 1, 0] },
  { "test": "sin 0", "expected": [3, 0] }
];

describe("Tokenizer test", () => {
  it("scan-numbers", () => {
    const s = new Scanner();

    for (const item of numbers) {
      const actual = s.tokenize(item.test);

      expect(actual.length === 1);
      expect(actual[0].value).toBe(item.expected);
    }
  })

  it("scan-expressions", () => {
    const s = new Scanner();

    for (const item of expressions) {
      const actual = s.tokenize(item.test);
      const expected = item.expected;

      const n = actual.length;
      expect(n === expected.length);

      for (let i = 0; i < n; i++) {
        expect.soft(actual[i].type).toBe(expected[i]);
      }
    }
  })

  it("scan-expressions-invalid", () => {
    const s = new Scanner();

    for (const item of invalid) {
      const actual = s.tokenize(item.test);
      const expected = item.expected;

      const n = actual.length;
      expect(n === expected.length);

      for (let i = 0; i < n; i++) {
        expect.soft(actual[i].type).toBe(expected[i]);
      }
    }
  })

  it('throws-on-invalid-input', () => {
    const s = new Scanner();

    expect(() => {
      s.tokenize('1+1#');
    }).toThrow('Unsupported input character');

    expect(() => {
      s.tokenize('(1+1');
    }).toThrow('Unbalanced parenthesis');

    expect(() => {
      s.tokenize('1+1)');
    }).toThrow('Unbalanced parenthesis');
  })
})
