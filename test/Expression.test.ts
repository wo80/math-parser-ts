import { describe, expect, it } from 'vitest'
import Parser from '../src/parser/Parser'

const expression1: { test: string, x: number, js: string }[] = [
  { "test": "sin(pi/2*x)", "x": 0.5, "js": "Math.sin(Math.PI/2*x)" },
  { "test": "sqrt(4*x^2)", "x": 3, "js": "Math.sqrt(4*Math.pow(x,2))" },
  { "test": "2^x", "x": 3, "js": "Math.pow(2,x)" },
  { "test": "2-3^x", "x": 4, "js": "2-Math.pow(3,x)" },
  { "test": "-2-3^x", "x": 4, "js": "-2-Math.pow(3,x)" },
  //{ "test": "-3^x", "x": 4, "js": "-Math.pow(3,x)" },
  //{ "test": "(-3)^x", "x": 4, "js": "Math.pow(-3,x)" },
  { "test": "1+x+x^2/2+x^3/6+x^4/24", "x": 2, "js": "1+x+Math.pow(x,2)/2+Math.pow(x,3)/6+Math.pow(x,4)/24" },
  { "test": "sin(x^2)", "x": 2, "js": "Math.sin(Math.pow(x,2))" },
  { "test": "sin(x)^2+cos(x)^2", "x": 2, "js": "Math.pow(Math.sin(x),2)+Math.pow(Math.cos(x),2)" }
];

const expression2: { test: string, x: number, y: number, js: string }[] = [
  { "test": "abs(sin(2*x*y))/(2*cos(x*y) + 1)", "x": 1, "y": 1, "js": "Math.abs(Math.sin(2*x*y))/(2*Math.cos(x*y)+1)" },
  { "test": "x*y + x^2 - y", "x": 0.2, "y": 0.5, "js": "x*y+Math.pow(x,2)-y" }
];

const simplify: { test: string, x: number, js: string }[] = [
  { "test": "1+sin(pi/2)-max(1,2)", "x": 0, "js": "1+Math.sin(Math.PI/2)-Math.max(1,2)" },
  { "test": "1+sin(pi/2)-pow(x,2)", "x": 1, "js": "1+Math.sin(Math.PI/2)-Math.pow(x,2)" },
  { "test": "sin(pi/2*x)", "x": 1, "js": "Math.sin(Math.PI/2*x)" }
];


describe("Expression test", () => {
  it("expression-1", () => {
    const p = new Parser({ autoVariables: true });
    for (const item of expression1) {
      const f = new Function('x', 'return ' + item.js + ';');
      expect(p.parse(item.test).evaluate({ x: item.x })).toBe(f(item.x));
    }
  })

  it("expression-2", () => {
    const p = new Parser({ autoVariables: true });
    for (const item of expression2) {
      const f = new Function('x', 'y', 'return ' + item.js + ';');
      expect(p.parse(item.test).evaluate({ x: item.x, y: item.y })).toBe(f(item.x, item.y));
    }
  })

  it("expression-simplify-1", () => {
    const p = new Parser({ autoVariables: true });
    for (const item of simplify) {
      const e = p.parse(item.test).simplify();
      const f = new Function('x', 'return ' + item.js + ';');
      expect(e.evaluate({ x: item.x })).toBe(f(item.x));
    }
  })

  it("expression-simplify-2", () => {
    const p = new Parser();
    const e = p.parse("1+sin(pi/2)-max(1,2)").simplify();

    expect(e.tokens.length).toBe(1);
    expect(e.tokens[0].value).toBe(0);
  });

  it("expression-compiled-1", () => {
    const p = new Parser({ autoVariables: true });
    for (const item of expression1) {
      const e = p.parse(item.test);
      const f = new Function('x', 'return ' + item.js + ';');
      const g = e.compile(['x']);
      expect(g(item.x)).toBe(f(item.x));
    }
  });

  it("expression-compiled-2", () => {
    const p = new Parser({ autoVariables: true });
    for (const item of expression2) {
      const e = p.parse(item.test);
      const f = new Function('x', 'y', 'return ' + item.js + ';');
      const g = e.compile(['x', 'y']);
      expect(g(item.x, item.y)).toBe(f(item.x, item.y));
    }
  });

  it("expression-variables", () => {
    const p = new Parser({ autoVariables: true });
    const e = p.parse("a*cos(2*pi*omega*t + phi)");
    const e1 = e.simplify({ a: 1, omega: 1, phi: 0 });
    const e2 = e.simplify({ a: 2, omega: 2, phi: Math.PI / 2 });

    //expect(e.variables.length).toBe(4);
    //expect(e1.variables.length).toBe(1);
    //expect(e2.variables.length).toBe(1);

    expect(e1.evaluate({ t: 0.5 })).toBe(e.evaluate({ a: 1, omega: 1, phi: 0, t: 0.5 }));
    expect(e2.evaluate({ t: 0.5 })).toBe(e.evaluate({ a: 2, omega: 2, phi: Math.PI / 2, t: 0.5 }));
  });

  it("expression-extend-1", () => {
    const p = new Parser({ autoVariables: true });

    p.constants['one'] = 1;
    p.extend('two', () => 2);
    
    expect('two' in Math).toBe(true);

    const e = p.parse("x + one + two()");
    const f = e.compile(['x']);

    expect(e.evaluate({ x: 0 })).toBe(3);
    expect(f(0)).toBe(3);
  });

  it("expression-extend-2", () => {
    const p = new Parser({ autoVariables: true });

    p.extend('max3', (a, b, c) => Math.max(a, Math.max(b, c)));
    p.extend('min3', (a, b, c) => Math.min(a, Math.min(b, c)));

    expect('max3' in Math).toBe(true);
    expect('min3' in Math).toBe(true);

    let e = p.parse("x + max3(1,2,3)");
    let f = e.simplify();
    let g = e.compile(['x']);

    expect(e.evaluate({ x: 0 })).toBe(3);
    expect(f.evaluate({ x: 0 })).toBe(3);
    expect(g(0)).toBe(3);

    e = p.parse("1 + max3(x,2,min3(3,4,y))");
    f = e.simplify({ y: 5 });
    g = e.compile(['x', 'y']);

    expect(e.evaluate({ x: 0, y: 5 })).toBe(4);
    expect(f.evaluate({ x: 0, y: 5 })).toBe(4);
    expect(g(0, 5)).toBe(4);
  });

  it("expression-extend-scope", () => {
    const p = new Parser({ autoVariables: true });

    p.functions['max2'] = (a: number, b: number) => a > b ? a : b;

    const e = p.parse("x + max2(1,2)");
    const f = e.simplify();

    expect('max2' in Math).toBe(false);

    expect(e.evaluate({ x: 0 })).toBe(2);
    expect(f.evaluate({ x: 0 })).toBe(2);

    /* User defined function max2 is not in Math neither in global scope. */
    expect(() => {
      const g = e.compile(['x']);
      const _ = g(0);
    }).toThrow('max2 is not defined');
  })

  it('throws-on-invalid-input', () => {
    const p = new Parser({ autoVariables: true });

    expect(() => {
      const e = p.parse('2x+1');
      const _ = e.evaluate({ x: 0 });
    }).toThrow('Error evaluating postfix expression');
  })
})
