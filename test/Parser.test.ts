import { describe, expect, it } from 'vitest'
import Parser from '../src/parser/Parser'

const integers: { test: string, js: string }[] = [
  { "test": "1+1", "js": "1+1" },
  { "test": "-2+4", "js": "-2+4" },
  { "test": "2-1", "js": "2-1" },
  { "test": "2*2", "js": "2*2" },
  { "test": "4/2", "js": "4/2" },
  { "test": "3^2", "js": "Math.pow(3,2)" },
  { "test": "3^-2", "js": "Math.pow(3,-2)" },
  { "test": "3^2^3", "js": "Math.pow(3,Math.pow(2,3))" },
  { "test": "(3^2)^3", "js": "Math.pow(Math.pow(3,2),3)" },
  { "test": "1+2-3", "js": "1+2-3" },
  { "test": "1-2+3", "js": "1-2+3" },
  { "test": "4-2-2", "js": "4-2-2" },
  { "test": "3*2-5", "js": "3*2-5" },
  { "test": "-5+2*2", "js": "-5+2*2" },
  { "test": "10/5*2", "js": "10/5*2" },
  { "test": "5*2/10", "js": "5*2/10" },
  { "test": "10/5+2", "js": "10/5+2" },
  { "test": "2+10/5", "js": "2+10/5" },
  { "test": "3*-2", "js": "3*-2" },
  { "test": "--2---2", "js": "-(-2)-(-(-2))" },
  { "test": "-(2+1)+4", "js": "-(2+1)+4" },
  { "test": "9/-(2+1)", "js": "9/-(2+1)" },
  { "test": "2*(3-1)", "js": "2*(3-1)" },
  { "test": "(3-1)*2", "js": "(3-1)*2" },
  { "test": "4/(3-1)", "js": "4/(3-1)" },
  { "test": "(3+1)/2", "js": "(3+1)/2" },
  { "test": "(1+1)*(3-1)", "js": "(1+1)*(3-1)" },
  { "test": "(3+1)/(3-1)", "js": "(3+1)/(3-1)" }
];

const numbers: { test: string, js: string }[] = [
  { "test": "1.5+.25", "js": "1.5+0.25" },
  { "test": ".25+1.5", "js": "0.25+1.5" },
  { "test": "-.25+0.75", "js": "-0.25+0.75" },
  { "test": "0.75-.25", "js": "0.75-0.25" },
  { "test": ".75-0.25", "js": "0.75-0.25" },
  { "test": "0.5*1.5", "js": "0.5*1.5" },
  { "test": ".1*.2", "js": "0.1*0.2" },
  { "test": "1.5/0.5", "js": "1.5/0.5" },
  { "test": ".05/.5", "js": "0.05/0.5" },
  { "test": "1.2^2", "js": "Math.pow(1.2,2)" },
  { "test": "9^.5", "js": "Math.pow(9,0.5)" },
  { "test": "1.2*2.5/.2", "js": "1.2*2.5/.2" },
  { "test": "1.2/2.5/3.2", "js": "1.2/2.5/3.2" },
  { "test": "1.2*-2.5+-3.2", "js": "1.2*-2.5+-3.2" },
  { "test": "1.2*.5^2", "js": "1.2*Math.pow(0.5,2)" },
  { "test": "1.2*.5^2/0.5", "js": "1.2*Math.pow(0.5,2)/0.5" },
  { "test": "(2.4*.5)/1.25", "js": "(2.4*.5)/1.25" },
  { "test": "(1.2+.8)^-2", "js": "Math.pow(1.2+.8,-2)" },
  { "test": "(1.2+.8)^-2*4", "js": "Math.pow(1.2+.8,-2)*4" },
  { "test": "1.2*.5^2/(1/3)", "js": "1.2*Math.pow(0.5,2)/(1/3)" }
];

const functions: { test: string, js: string }[] = [
  { "test": "cos(1)", "js": "Math.cos(1)" },
  { "test": "sin(1)", "js": "Math.sin(1)" },
  { "test": "tan(1)", "js": "Math.tan(1)" },
  { "test": "acos(1)", "js": "Math.acos(1)" },
  { "test": "asin(1)", "js": "Math.asin(1)" },
  { "test": "atan(1)", "js": "Math.atan(1)" },
  { "test": "atan2(1, 2)", "js": "Math.atan2(1, 2)" },
  { "test": "cosh(1)", "js": "Math.cosh(1)" },
  { "test": "sinh(1)", "js": "Math.sinh(1)" },
  { "test": "tanh(1)", "js": "Math.tanh(1)" },
  { "test": "acosh(1)", "js": "Math.acosh(1)" },
  { "test": "asinh(1)", "js": "Math.asinh(1)" },
  { "test": "atanh(1)", "js": "Math.atanh(1)" },
  { "test": "hypot(2, 3)", "js": "Math.hypot(2, 3)" },
  { "test": "exp(1)", "js": "Math.exp(1)" },
  { "test": "log(6)", "js": "Math.log(6)" },
  { "test": "log2(6)", "js": "Math.log2(6)" },
  { "test": "log10(6)", "js": "Math.log10(6)" },
  { "test": "sqrt(2)", "js": "Math.sqrt(2)" },
  { "test": "cbrt(2)", "js": "Math.cbrt(2)" },
  { "test": "abs(-1)", "js": "Math.abs(-1)" },
  { "test": "pow(2, 3)", "js": "Math.pow(2, 3)" },
  { "test": "min(2, 3)", "js": "Math.min(2, 3)" },
  { "test": "max(2, 3)", "js": "Math.max(2, 3)" }
];

const functions2: { test: string, js: string }[] = [
  { "test": "sin(pi/2)", "js": "Math.sin(Math.PI/2)" },
  { "test": "cos(pi/2)", "js": "Math.cos(Math.PI/2)" },
  { "test": "tan(pi/4)", "js": "Math.tan(Math.PI/4)" },
  { "test": "2*exp(1)-e", "js": "2*Math.exp(1)-Math.E" },
  { "test": "log(e)", "js": "Math.log(Math.E)" },
  { "test": "sin(-pi/2)", "js": "Math.sin(-Math.PI/2)" },
  { "test": "-sin(pi/2)", "js": "-Math.sin(Math.PI/2)" },
  { "test": "-sin(-pi/2)", "js": "-Math.sin(-Math.PI/2)" },
  { "test": "sin(cos(1))", "js": "Math.sin(Math.cos(1))" },
  { "test": "cos(-sin(1))", "js": "Math.cos(-Math.sin(1))" },
  { "test": "-cos(sin(-1))", "js": "-Math.cos(Math.sin(-1))" }
];

describe("Parser test", () => {
  it("parse-integers", () => {
    for (const item of integers) {
      expect(Parser.eval(item.test)).toBe(eval(item.js));
    }
  })

  it("parse-numbers", () => {
    for (const item of numbers) {
      expect(Parser.eval(item.test)).toBe(eval(item.js));
    }
  })

  it("parse-functions", () => {
    for (const item of functions) {
      expect(Parser.eval(item.test)).toBe(eval(item.js));
    }
  })

  it("parse-functions-2", () => {
    for (const item of functions2) {
      expect(Parser.eval(item.test)).toBe(eval(item.js));
    }
  })

  it("parse-extended-1", () => {
    const p = new Parser();

    // Add alias for log functions (ISO notation).
    p.functions['ln'] = Math.log;
    p.functions['lb'] = Math.log2;
    p.functions['lg'] = Math.log10;

    expect(p.parse("ln(2)").evaluate()).toBe(Math.log(2));
    expect(p.parse("lb(3)").evaluate()).toBe(Math.log2(3));
    expect(p.parse("lg(4)").evaluate()).toBe(Math.log10(4));
  })

  it("parse-extended-2", () => {
    const p = new Parser();

    p.constants['one'] = 1;

    // Anonymous function (add to 'Math' object).
    p.functions['two'] = () => 2;

    const e = p.parse("one + two()");

    //expect(Math.hasOwnProperty('two')).toBe(true);
    expect(e.evaluate()).toBe(3);
  })

  it("parse-questionable-syntax", () => {
    const p = new Parser();

    /* Should the following expressions throw? */
    expect(p.parse('(1+1)()').evaluate()).toBe(2);
  })

  it('throws-on-invalid-input', () => {
    const p = new Parser();

    expect(() => {
      p.parse('1+sin+2');
    }).toThrow('Undefined symbol \'sin\' at position 2');

    expect(() => {
      p.parse('(1+sin)(0)');
    }).toThrow('Undefined symbol \'sin\' at position 3');

    expect(() => {
      p.parse('1+gammma(1)');
    }).toThrow('Undefined function \'gammma\' at position 2');

    expect(() => {
      p.parse(')1+1(');
    }).toThrow('Unbalanced parenthesis');
  })
})
