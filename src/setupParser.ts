import Parser from './parser/Parser'

export function setupParser(input: HTMLInputElement, output: HTMLDivElement) {
  const p = new Parser();
  const callParser = () => {
    try {
      const value = p.parse(input.value).evaluate();
      output.innerText = value.toString();
    } catch (error) {
      output.innerText = error.toString();
    }
  }
  input.addEventListener('input', () => callParser())
  callParser()
}
