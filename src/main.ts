import './style.css'
import { setupParser } from './setupParser.ts'

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <div>
    <h1>math-parser-ts</h1>
    <div class="card">
      <input id="input" type="text" value="1+1"></input>
      <div id="output"></div>
    </div>
    <p class="read-the-docs">
      Enter a math expression in the input field above.
    </p>
  </div>
`

setupParser(document.querySelector<HTMLInputElement>('#input')!, document.querySelector<HTMLDivElement>('#output')!)
