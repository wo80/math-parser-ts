
enum TokenType { Operand, Operator, Parenthesis, Symbol, Function, Separator }

export default TokenType;
