﻿import TokenType from './TokenType';

export default class Token {
  /** Token type. */
  public type: TokenType;

  /** Token name. */
  public text: string;

  /** Position. */
  public position: number;

  /** Numeric value. */
  public value: number;

  /**
   * Creates a token.
   *
   * @param type ???
   * @param text ???
   * @param position ???
   * @param value ???
   * @returns Token.
   */
  constructor(type: TokenType, text: string, position: number, value: number = 0) {
    this.type = type;
    this.text = text;
    this.position = position;
    this.value = value;
  }

  static operand(text: string, position: number, value: number): Token {
    return new Token(TokenType.Operand, text, position, value);
  }

  static operator(text: string, position: number): Token {
    return new Token(TokenType.Operator, text, position);
  }

  static paren(text: string, position: number): Token {
    return new Token(TokenType.Parenthesis, text, position);
  }

  static symbol(text: string, position: number): Token {
    return new Token(TokenType.Symbol, text, position);
  }

  static func(text: string, position: number): Token {
    return new Token(TokenType.Function, text, position);
  }

  static separator(text: string, position: number): Token {
    return new Token(TokenType.Separator, text, position);
  }

  clone() {
    return new Token(this.type, this.text, this.position, this.value);
  }

  toString() {
    return this.text;
  }
}
