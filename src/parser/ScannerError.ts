﻿
export default class ScannerError extends Error {
  /** Position. */
  public position: number;

  constructor(message: string, position: number) {
    super(message);
    this.position = position;

    Object.setPrototypeOf(this, ScannerError.prototype);
  }
}
