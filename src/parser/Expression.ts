﻿import ExpressionCompiler from './ExpressionCompiler';
import Operator from './Operator';
import ParserError from './ParserError';
import Token from './Token';
import TokenType from './TokenType';

export default class Expression {
  /// <summary>
  /// Gets or sets a value indicating to how many decimal places the expression
  /// evaluation result should be rounded (default = 0, no rounding).
  /// </summary>
  public round: number;
  public tokens: Token[];

  private functions: { [i: string]: (...args: number[]) => number };
  private variables?: string[];

  constructor(tokens: Token[], functions: { [i: string]: (...args: number[]) => number }, variables?: string[]) {
    this.tokens = tokens;
    this.variables = variables;
    this.functions = functions;
    this.round = 0;
  }

  /// <summary>
  /// Simplify the expression.
  /// </summary>
  /// <remarks>
  /// Be careful using the <c>Simplify</c> method when using user-defined functions. For example, when
  /// registering a function 'time()' which is supposed to return the current Unix time-stamp, evaluation
  /// of the expression will actually always return the time-stamp when <c>Simplify</c> was called.
  /// </remarks>
  simplify(variables?: { [i: string]: number }): Expression {
    const functions = this.functions;

    const stack: Token[] = [];

    for (const token of this.tokens) {
      if (token.type === TokenType.Operand) {
        stack.push(token);
      }
      else if (token.type === TokenType.Operator) {
        if (Operator.isBinaryOperator(token.text)) {
          const op2 = stack.pop()!;
          const op1 = stack.pop()!;

          if (op1.type === TokenType.Operand && op2.type === TokenType.Operand) {
            const text = `[${op1.text} ${op2.text} ${token.text}]`;
            const value = Operator.evaluate2(token.text, op1.value, op2.value);

            stack.push(Token.operand(text, token.position, value));
          }
          else {
            stack.push(op1, op2, token);
          }
        }
        else {
          const op = stack.pop()!;

          if (op.type === TokenType.Operand) {
            const text = `[${op.text} ${token.text}]`;
            const value = Operator.evaluate(token.text, op.value);

            stack.push(Token.operand(text, token.position, value));
          }
          else {
            stack.push(op, token);
          }
        }
      }
      else if (token.type === TokenType.Function) {
        const fn = functions[token.text];
        const order = fn.length;

        if (order === 0) {
          stack.push(Token.operand(`[${token.text}]`, token.position, fn()));
        } else {
          const ops = stack.splice(-order);

          if (ops.every(t => t.type === TokenType.Operand)) {
            const text = [...ops.map(t => t.text), token.text].join(' ');
            const args = ops.map(t => t.value);
            stack.push(Token.operand(`[${text}]`, token.position, fn(...args)));
          } else {
            stack.push(...ops, token);
          }
        }
      }
      else if (token.type === TokenType.Symbol && variables?.hasOwnProperty(token.text)) {
        stack.push(Token.operand(token.text, token.position, variables[token.text]));
      }
      else {
        stack.push(token);
      }
    }

    return new Expression(stack, this.functions, this.variables);
  }

  /// <summary>
  /// Evaluate the expression.
  /// </summary>
  evaluate(variables?: { [i: string]: number }): number {
    const length: number = this.tokens.length;

    if (length === 0) {
      throw new Error();
    }

    const functions = this.functions;

    const stack: number[] = [];

    for (let i = 0; i < length; i++) {
      const token = this.tokens[i];

      if (token.type === TokenType.Operand) {
        stack.push(token.value);
      }
      else if (token.type === TokenType.Operator) {
        if (Operator.isBinaryOperator(token.text)) {
          const op2 = stack.pop()!;
          const op1 = stack.pop()!;
          stack.push(Operator.evaluate2(token.text, op1, op2));
        }
        else {
          const op = stack.pop()!;
          stack.push(Operator.evaluate(token.text, op));
        }
      }
      else if (token.type === TokenType.Function) {
        const fn = functions[token.text];
        const order = fn.length;

        if (order === 0) {
          stack.push(fn());
        } else {
          const args = stack.splice(-order);
          stack.push(fn(...args));
        }
      }
      else if (variables && token.type === TokenType.Symbol) {
        stack.push(variables[token.text]);
      }
      else {
        throw new ParserError('Unknown token', token);
      }
    }

    if (stack.length !== 1) {
      throw new ParserError('Error evaluating postfix expression');
    }

    if (this.round > 0) {
      const e = Math.pow(10, this.round);
      return Math.round((stack[0] + Number.EPSILON) * e) / e;
    }

    return stack[0];
  }

  compile(args?: string[]): Function {
    return new ExpressionCompiler().create(this.tokens, this.functions, args || this.variables!);
  }

  /// <summary>
  /// Returns a string that represents the postfix expression.
  /// </summary>
  toString(): string {
    return this.tokens.map(t => t.text).join(' ');
  }
}
