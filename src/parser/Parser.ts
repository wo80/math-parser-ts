﻿import TokenType from "./TokenType";
import Token from "./Token";
import Operator from "./Operator";
import ParserConfig from "./ParserConfig";
import ParserError from "./ParserError";
import Expression from "./Expression";
import Scanner from "./Scanner";

function defaultFunctions(): { [i: string]: (...args: number[]) => number } {
  const NAMES: string[] = ['cos', 'sin', 'tan', 'acos', 'asin', 'atan', 'atan2',
    'cosh', 'sinh', 'tanh', 'acosh', 'asinh', 'atanh', 'hypot', 'exp', 'log',
    'log2', 'log10', 'sqrt', 'cbrt', 'abs', 'pow', 'min', 'max'];

  const funcs: { [i: string]: (...args: number[]) => number } = {};

  for (const i in NAMES) {
    const name = NAMES[i];
    const desc = Object.getOwnPropertyDescriptor(Math, name);
    if (desc) {
      funcs[name] = desc.value;
    }
  }
  return funcs;
}

export default class Parser {
  public functions: { [i: string]: (...args: number[]) => number };
  public constants: { [i: string]: number };
  public variables: string[];

  public autoVariables: boolean;

  constructor(config?: ParserConfig) {
    this.functions = config?.functions || defaultFunctions();
    this.constants = config?.constants || { pi: Math.PI, e: Math.E };
    this.autoVariables = config?.autoVariables || false;
    this.variables = [];
  }

  static eval(expression: string): number {
    return new Parser().parse(expression).evaluate();
  }

  extend(name: string, fn: (...a: number[]) => number): void {
    this.functions[name] = fn;

    // Extend the Math object (don't overwrite existing functions).
    if (!(name in Math)) {
      Object.defineProperty(Math, name, { value: fn });
    }
  }

  parse(expression: string, variables?: string[]): Expression {
    if (!expression) {
      throw new ParserError('Input expression cannot be empty');
    }

    if (variables) {
      for (const name of variables) {
        if (name in this.constants) {
          throw new ParserError('Variable name already registered as a constant: ' + name);
        }
        if (name in this.functions) {
          throw new ParserError('Variable name already registered as a function: ' + name);
        }
        if (!this.variables.includes(name)) {
          this.variables.push(name);
        }
      }
    }

    const scanner = new Scanner();

    return new Expression(this.toPostfix(scanner.tokenize(expression)), this.functions, this.variables);
  }

  // The Shunting-Yard algorithm:

  /// <summary>
  /// Converts the input token array in infix notation to postfix notation.
  /// </summary>
  toPostfix(input: Token[]): Token[] {
    const length = input.length;

    if (input[length - 1].type === TokenType.Operator) {
      // We don't support postfix operators, so the last token can never be one.
      throw new ParserError('Invalid postfix operator', input[length - 1]);
    }

    // The output stack.
    const output: Token[] = [];

    // Temporary operator stack.
    const operators: Token[] = [];

    // For all tokens in the array
    for (let i = 0; i < length; i++) {
      const token = input[i];

      if (token.type === TokenType.Operand) {
        // Encountered two successive operands.
        if (i > 0 && input[i - 1].type === TokenType.Operand) {
          throw new ParserError('Invalid operand token', token);
        }

        // Enqueue operand
        output.push(token);
      }
      else if (token.type === TokenType.Symbol) {
        if (token.text in this.constants) {
          output.push(Token.operand(token.text, token.position, this.constants[token.text]));
        }
        else if (this.variables.includes(token.text)) {
          output.push(token);
        }
        else if (this.autoVariables) {
          if (token.text in this.functions) {
            throw new ParserError('Symbol name already registered as a function: ' + token.text);
          }

          this.variables.push(token.text);

          output.push(token);
        }
        else {
          throw new ParserError(`Undefined symbol '${token.text}' at position ${token.position}`, token);
        }
      }
      else if (token.type === TokenType.Function) {
        if (token.text in this.functions) {
          operators.push(token);
        }
        else {
          throw new ParserError(`Undefined function '${token.text}' at position ${token.position}`, token);
        }
      }
      else if (token.type === TokenType.Operator) {
        if (Operator.isUnaryMinus(token.text)) {
          operators.push(token);
          continue;
        }

        if (operators.length > 0) {
          const a = Operator.getPrecedence(token.text);
          const leftAssociative = Operator.isLeftAssociative(token.text);

          let b = Operator.getPrecedence(operators.slice(-1)[0].text);

          while (operators.length > 0 && (a < b || (a === b && leftAssociative))) {
            // Pop operator from stack and enqueue
            output.push(operators.pop()!);

            if (operators.length > 0) {
              b = Operator.getPrecedence(operators.slice(-1)[0].text);
            }
          }
        }

        // Push operator to stack
        operators.push(token);
      }
      else if (token.type === TokenType.Separator) {
        // Pop operators from stack until there is an opening parenthesis
        while (operators.length > 0 && operators.slice(-1)[0].text !== '(') {
          output.push(operators.pop()!);
        }
      }
      else if (token.text === '(') {
        // Push to stack
        operators.push(token);
      }
      else if (token.text === ')') {
        // Pop operators from stack until there is an opening parenthesis
        while (operators.length > 0) {
          const t = operators.slice(-1)[0];

          if (t.type === TokenType.Parenthesis && t.text === '(') {
            operators.pop();
            break;
          }

          output.push(operators.pop()!);
        }

        // Check if a function is on top the stack
        if (operators.length > 0 && operators.slice(-1)[0].type === TokenType.Function) {
          output.push(operators.pop()!);
        }
      }
    }

    // Pop the remaining operators from stack and enqueue them
    while (operators.length > 0) {
      const op = operators.pop()!;

      if (op.type === TokenType.Parenthesis) {
        throw new ParserError('Invalid parenthesis', op);
      }

      output.push(op);
    }

    return output;
  }
}