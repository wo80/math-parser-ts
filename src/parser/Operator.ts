﻿
export default class Operator {
  /// <summary>
  /// Returns true, whether the operation can be handled by the parser.
  /// </summary>
  static isKnownOperator(op: string): boolean {
    switch (op) {
      case '+':
      case '-':
      case '/':
      case '*':
      case '^': return true;
    }

    return false;
  }

  /// <summary>
  /// Returns whether the operator is left-associative.
  /// </summary>
  static isLeftAssociative(op: string): boolean { return op !== '^'; }

  /// <summary>
  /// Returns whether the operator is right-associative.
  /// </summary>
  static isRightAssociative(op: string): boolean { return op === '^'; }

  /// <summary>
  /// Returns the priority of the given operator.
  /// </summary>
  static getPrecedence(op: string): number {
    switch (op) {
      case '(': return 1; // Include left paren to simplify parsing.
      case '+': return 4;
      case '-': return 4;
      case '/': return 6;
      case '*': return 6;
      case '^': return 8;
      case '~': return 9; // Unary minus.
    }

    return 0;
  }

  /// <summary>
  /// Evaluate a unary operator.
  /// </summary>
  static evaluate(o: string, op: number): number {
    switch (o[0]) {
      case '~': return -op;
    }

    return Number.NaN;
  }

  /// <summary>
  /// Evaluates a binary operator.
  /// </summary>
  static evaluate2(o: string, op1: number, op2: number): number {
    switch (o[0]) {
      case '+': return op1 + op2;
      case '-': return op1 - op2;
      case '/': return op1 / op2;
      case '*': return op1 * op2;
      case '^': return Math.pow(op1, op2);
    }

    return Number.NaN;
  }

  /// <summary>
  /// Returns whether the operator is a binary operator.
  /// </summary>
  static isBinaryOperator(op: string): boolean {
    switch (op[0]) {
      case '+':
      case '-':
      case '/':
      case '*':
      case '^': return true;
      case '~': return false; // Unary minus
    }

    return true;
  }

  /// <summary>
  /// Returns true, whether the operation can be handled by the parser.
  /// </summary>
  static isUnaryPrefixOperator(op: string): boolean { return op === '-' || op === '~'; }

  /// <summary> 
  /// Gets a value indicating whether the token is a unary minus.
  /// </summary>
  static isUnaryMinus(op: string): boolean { return op === '~'; }
}
