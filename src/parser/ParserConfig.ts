
export default interface ParserConfig {
  constants?: { [i: string]: number },
  functions?: { [i: string]: (...args: number[]) => number },
  autoVariables?: boolean
}
