﻿import TokenType from "./TokenType";
import Token from "./Token";
import Operator from "./Operator";
import ScannerError from "./ScannerError";

function isWhiteSpace(c: number) { return c < 33; }

function isDigit(c: number) { return (c >= 48 && c <= 57); }

function isLetter(c: number) { return (c >= 97 && c <= 122); }

function isLetterOrDigit(c: number) { return (isLetter(c) || isDigit(c)); }

// Regular expression to match floating point numbers.
const regex = /\d*\.?\d+([eE][-+]?\d+)?/;

export default class Scanner {
  //private decimalSeperator: string;
  private position: number;

  constructor() {
    //this.decimalSeperator = '.';
    this.position = 0;
  }

  /// <summary>
  /// Static function, which splits the input expression into a token array.
  /// </summary>
  /// <param name="expression">The input math formula.</param>
  /// <returns>Array of <see cref="Token"/>s.</returns>
  /// <exception cref="ScannerException">
  /// Occurs, if an invalid character is found in the input string, parentheses
  /// are unbalanced or an operand could not be parsed to double.
  /// </exception>
  tokenize(expression: string): Token[] {
    this.position = 0;

    const token: Token[] = [];
    const length = expression.length;

    let pos = 0;

    // Counts opening and closing brackets
    let balance = 0;

    // Scan the input expression
    while (pos < length) {
      const c = expression.charCodeAt(pos);
      let s = expression.charAt(pos);

      if (isWhiteSpace(c)) {
        // Ignore whitespace
      }
      else if (Operator.isKnownOperator(s)) {
        // Detect unary prefix operators.
        if (Operator.isUnaryPrefixOperator(s)) {
          const count: number = token.length;

          if (count === 0
            || token[count - 1].type === TokenType.Operator
            || token[count - 1].type === TokenType.Separator
            || token[count - 1].text === '(') {
            // Unary minus is the only prefix operator.
            s = '~';
          }
        }

        token.push(Token.operator(s, pos));
      }
      else if (s === '(') {
        token.push(Token.paren(s, pos));
        balance++;
      }
      else if (s === ')') {
        balance--;

        if (balance < 0) {
          throw new ScannerError('Unbalanced parenthesis', pos);
        }

        /*
        const count: number = token.length;

        // Empty parenthesis are not allowed
        if (count === 0 || token[count - 1].text === '(')
        {
          throw new ScannerError('Invalid parenthesis', pos);
        }
        //*/

        token.push(Token.paren(s, pos));
      }
      else if (isDigit(c) || s === '.') {
        token.push(this.readNumber(expression, pos));
        pos = this.position;
      }
      else if (isLetter(c) || s === '_') {
        token.push(this.readSymbol(expression, length, pos));
        pos = this.position;
      }
      else if (s === ',') {
        token.push(Token.separator(s, pos));
      }
      else {
        throw new ScannerError('Unsupported input character', pos);
      }

      pos++;
    }

    if (balance != 0) {
      throw new ScannerError('Unbalanced parenthesis', pos - 1);
    }

    return token;
  }

  /// <summary>
  /// Reads a decimal number from the input expression.
  /// </summary>
  /// <returns>Token of type <see cref="TokenType.Operand"/>.</returns>
  readNumber(expression: string, pos: number): Token {
    const result = regex.exec(expression.substring(pos));

    if (!result || result.length === 0) {
      throw new ScannerError('Invalid floating point format', pos);
    }

    const s = result[0];

    this.position = pos + s.length - 1;

    return Token.operand(s, pos, parseFloat(s));
  }

  /// <summary>
  /// Reads a symbol (function, variable or constant) from the input expression.
  /// </summary>
  /// <returns>Token of type <see cref="TokenType.Symbol"/>.</returns>
  readSymbol(expression: string, length: number, pos: number): Token {
    let type: TokenType = TokenType.Symbol;

    const start = pos;
    let i = pos;

    // Find end of symbol name
    while (i < length && (isLetterOrDigit(expression.charCodeAt(i)) || expression[i] === '_')) i++;

    const text = expression.substring(pos, i);

    // There may be space between function name and arguments
    while (i < length && isWhiteSpace(expression.charCodeAt(i))) i++;

    this.position = i - 1;

    if (i < length && expression[i] === '(') {
      type = TokenType.Function;
    }

    return new Token(type, text, start);
  }
}
