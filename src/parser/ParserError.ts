﻿import Token from "./Token";

export default class ParserError extends Error {
  /** Token. */
  public token?: Token;

  constructor(message: string, token?: Token) {
    super(message);
    this.token = token;

    Object.setPrototypeOf(this, ParserError.prototype);
  }
}
