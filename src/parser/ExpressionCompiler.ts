﻿import Operator from "./Operator";
import ParserError from "./ParserError";
import Token from "./Token";
import TokenType from "./TokenType";

export default class ExpressionCompiler {
  create(expression: Token[], functions: { [i: string]: (...args: number[]) => number }, args: string[]): Function {
    return new Function(...args, 'return ' + this.toString(expression, functions) + ';');
  }

  toString(tokens: Token[], functions: { [i: string]: (...args: number[]) => number }): string {
    const length: number = tokens.length;

    if (length === 0) {
      throw new Error();
    }

    const stack: string[] = [];

    for (let i = 0; i < length; i++) {
      const token = tokens[i];

      if (token.type === TokenType.Operand) {
        stack.push(token.value.toString());
      }
      else if (token.type === TokenType.Operator) {
        const s = token.text;

        if (Operator.isBinaryOperator(s)) {
          const op2 = stack.pop();
          const op1 = stack.pop();
          if (s === '^') { // Power
            stack.push('Math.pow(' + op1 + ',' + op2 + ')');
          } else {
            stack.push('(' + op1 + s + op2 + ')');
          }
        }
        else {
          //if (s !== '~') error

          stack.push('(-' + stack.pop() + ')');
        }
      }
      else if (token.type === TokenType.Function) {
        let s = token.text;

        const fn = functions[s];
        const order = fn.length;

        // If the function is not in the Math object, we assume it's in the
        // scope the expression will be evaluated in.
        if (s in Math) {
          s = 'Math.' + s;
        }

        if (order === 0) {
          stack.push(s + '()');
        } else {
          const args = stack.splice(-order).reverse();
          stack.push(s + '(' + args.join(', ') + ')');
        }
      }
      else if (token.type === TokenType.Symbol) {
        stack.push(token.text);
      }
      else {
        throw new ParserError('Unknown token', token);
      }
    }

    if (stack.length !== 1) {
      throw new ParserError('Error evaluating postfix expression');
    }

    return stack[0];
  }
}
