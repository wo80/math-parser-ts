# Math Parser for Typescript

A simple math expression parser written in typescript. For details see

* [Math Expression Parser (1/2)](http://wo80.bplaced.net/posts/2020/03-math-parser-1/)
* [Math Expression Parser (2/2) - Optimization](http://wo80.bplaced.net/posts/2020/03-math-parser-2/)

A C# version is available [here](https://gitlab.com/wo80/math-parser).

## Examples

Evaluate math expression:

```ts
let value = Parser.eval("sin(pi/2)")
```

Using variables:

```ts
const p = new Parser({ autoVariables: true })
let e = p.parse("sin(pi/2*x)");
let value = e.evaluate({ x: 0 })
```

Compile expression:

```ts
const p = new Parser({ autoVariables: true })
let e = p.parse("x + y");
let f = e.compile(['x', 'y']);
let value = f(1.0, 1.0);
```

Take a look at the [unit tests](test/) for more examples.
